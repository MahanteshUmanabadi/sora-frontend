
import Register from './Features/Register/Register.vue';
import Dashboard from './Features/Dashboard/Dashboard.vue';
import ForgetPassword from './Features/ForgetPassword/ForgetPassword.vue';
import TheLogin from './Features/Login/TheLogin.vue'
import SubmitVerification from './Features/Register/SubmitVerification.vue'
import SubmitOtp from './Features/ForgetPassword/SubmitOtp.vue'
import LoginData from './Features/Users/LoginData.vue'


import DefaultLayout from "./Layout/DefaultLayout.vue";
export const routes = [
                {
                    path:'/',
                    name:'Home',
                    component:DefaultLayout,
                    meta: {
                        reload: true,
                    },
                },
            {
                path:'/register',
                name:'register',
                component:Register,
            },
            {
                path: '/register/verify/:email',
                name: 'submitverify',
                component: SubmitVerification,
                props:true,
            },
            {
                path:'/dashboard',
                name:'dashboard',
                component:Dashboard,
                beforeRouteEnter: (to, from, next) => {
                    let user = sessionStorage.getItem("user-info")
                    if(user){
                        from="/login"
                        next()
                    }else{
                        to="/login"
                    }
                },
                meta: {
                    reload: true,
                }
            },
            {
                path: '/login',
                name: 'login',
                component: TheLogin,
                
              
            },
            {
                path: '/usersLogged',
                name: 'usersLogged',
                component: LoginData,
                
              
            },
            {
                path:'/forget',
                name:'forgetpassword',
                component:ForgetPassword,
                children:[
                    
                   
                ]
            },
            {
                path:'/forget/otp',
                name:'submitotp',
                component:SubmitOtp,
                children:[
                ]
            }
        
    
];