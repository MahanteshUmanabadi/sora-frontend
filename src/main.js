import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import { routes } from "./routes";
import App from './App.vue'
import store from './store'
import VOtpInput from "vue3-otp-input";
import BaseDialog from "./UI/BaseDialog.vue";


const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

const app = createApp(App);

app.component('v-otp-input', VOtpInput);
app.component("base-dialog", BaseDialog);
app.use(store)
app.use(router)

app.mount('#app')

