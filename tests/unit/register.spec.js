import { mount } from '@vue/test-utils'

import Register from '../../src/Features/Register/Register.vue';

const wrapper=mount(Register)
test("check input of firstName",async ()=>{
    
    const firstName=wrapper.find('#firstName')
     
    await firstName.setValue('Abhilash')
    expect(firstName.element.value).toBe('Abhilash')
})

test('check input of LastName',async ()=>{
    const lastName=wrapper.find('#lastName')
    
    await lastName.setValue("Nyamagoud")
    expect(lastName.element.value).toBe("Nyamagoud")
})

test('check input of email',async ()=>{
    const email=wrapper.find('#email')

    await email.setValue("abhilash@gmail.com")
    expect(email.element.value).toBe('abhilash@gmail.com')

} )

test('check input of userName',async ()=>{
    const userName=wrapper.find('#userName')
    await userName.setValue("Abhilash@101")
    expect(userName.element.value).toBe('Abhilash@101')
})

test('check input of password',async ()=>{
    const password=wrapper.find('#password')
    await password.setValue("Abhilash@123")
    expect(password.element.value).toBe('Abhilash@123')
})

test('check input of phone',async()=>{
    const phone=wrapper.find('#phone')
    await phone.setValue('8792655457')
    expect(phone.element.value).toBe('8792655457')
})

// test('trigger',async()=>{
//     await wrapper.find('button').trigger('click')
//     expect(wrapper.emitted()).toHaveProperty('submitReg')
// })

// test('form submit',async()=>{
//     const wrapper=mount(Register)
//     const firstName='Abhilash'
//     const lastName="Nyamagoud"
//     const email="abhilash@gmail.com"
//     const userName="Abhi@101"
//     const password="Hello@123"
//     const phone='8792655458'
         
//     await wrapper.find('#firstName').setValue(firstName)
//     await wrapper.find('#lastName').setValue(lastName)
//     await wrapper.find('#email').setValue(email)
//     await wrapper.find('#userName').setValue(userName)
//     await wrapper.find('#password').setValue(password)
//     await wrapper.find('#phone').setValue(phone)
 
    
//     await wrapper.find('button').trigger('click')
//     expect(wrapper.emitted('handleSubmit')).toBe({
//         firstName,
//         lastName,
//         email,
//         userName,
//         password,
//         phone
//     })
// })