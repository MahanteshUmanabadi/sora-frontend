import {  shallowMount } from '@vue/test-utils'
import "bootstrap/dist/css/bootstrap.min.css"
import "vue3-otp-input";
import { createStore } from 'vuex';

import ForgetPassword from '../../src/Features/ForgetPassword/ForgetPassword.vue'
import SubmitOtp from '../../src/Features/ForgetPassword/SubmitOtp.vue'

describe('Forget Password - email input page',()=>{
    const createVuexStore = (initialState) =>
        createStore({
            state: {
                users:{
                    email: '',
                    otp: ''
                },
                ...initialState
            },
            mutations: {
                setData(state,users) {
                  state.users = state.users
                }
            }
        })

    const forgetwrapper = shallowMount(ForgetPassword)

    test('test card', async () => {
        const cardhead = forgetwrapper.find('.card-header')
        const cardbody = forgetwrapper.find('#emailinputlabel')
        expect(cardhead.text()).toBe('Reset Password')
        expect(cardbody.text()).toBe('Email')
    })

    test('test email input', async () => {
        const input = forgetwrapper.find('#resetemailinput')
        await input.setValue('my@mail.com')
        expect(input.element.value).toBe('my@mail.com')
    })

    test('test submit button text', async () => {
        const submitbtn = forgetwrapper.find('button')
        expect(submitbtn.text()).toBe('Send OTP')
    })
    
    test('test vuex', async() => {
        const store = createVuexStore({  
                users:{
                    email: 'shyl@g.com',
                    otp: '6666'
                } 
        })
        store.commit('setData')
        expect(store.state.users.email).toBe('shyl@g.com')
    })
})


describe('Forget password - OTP input page',()=>{
    const otpwrapper = shallowMount(SubmitOtp)
    test('test card', async () => {
        const cardhead = otpwrapper.find('.card-header')  
        expect(cardhead.text()).toBe('Submit OTP')

        const otptext = wrapper.find('#otpinputlabel')
        const cardbody1 = wrapper.find('#resetpasswordlabel')
        const cardbody2 = wrapper.find('#confirmpasswordlabel')

        expect(otptext.text()).toBe('OTP')
        expect(cardbody1.text()).toBe('New Password')
        expect(cardbody2.text()).toBe('Confirm Password')
    })
    test('test otp input', async () => {
        const newpasswordinput = otpwrapper.find('#otpinput')
        await newpasswordinput.setValue('1234')
        expect(newpasswordinput.element.value).toBe('1234')
    })

    test('test new password input', async () => {
        const newpasswordinput = otpwrapper.find('#resetpasswordinput')
        await newpasswordinput.setValue('Abc12345678')
        expect(newpasswordinput.element.value).toBe('Abc12345678')
    })

    test('test confirm password input', async () => {
        const confirmpasswordinput = otpwrapper.find('#confirmpasswordinput')
        await confirmpasswordinput.setValue('Abc12345678')
        expect(confirmpasswordinput.element.value).toBe('Abc12345678')
    })
})
